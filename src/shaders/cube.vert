#version 460 core

#include <src/shaders/common.all>
#include <src/shaders/common.vert>

layout (location=0) out vec3 direction_vector;

const vec3 local_positions[8] = vec3[8](
  vec3(-1.0,-1.0, 1.0),
  vec3( 1.0,-1.0, 1.0),
  vec3( 1.0, 1.0, 1.0),
  vec3(-1.0, 1.0, 1.0),

  vec3(-1.0,-1.0,-1.0),
  vec3( 1.0,-1.0,-1.0),
  vec3( 1.0, 1.0,-1.0),
  vec3(-1.0, 1.0,-1.0)
);

const int indices[36] = int[36](
  // front
  0, 1, 2, 2, 3, 0,
  // right
  1, 5, 6, 6, 2, 1,
  // back
  7, 6, 5, 5, 4, 7,
  // left
  4, 0, 3, 3, 7, 4,
  // bottom
  4, 5, 1, 1, 0, 4,
  // top
  3, 2, 6, 6, 7, 3
);

void main() {
  // set gl_Position = clip space position
  int index = indices[gl_VertexID];
  gl_Position = mvp_matrix * vec4(local_positions[index], 1.0);

  direction_vector = local_positions[index].xyz;
}
