layout(std430, binding = 0) restrict readonly buffer LocalVerticesDataBuffer {
  LocalVertexData local_vertices_data[];
};

// @param vid = vertex id
// @return local position for a given vertex id
vec3 get_position_at_vid(int vid) {
  return vec3(local_vertices_data[vid].position[0], local_vertices_data[vid].position[1], local_vertices_data[vid].position[2]);
}

// @param vid = vertex id
// @return local normal vector for a given vertex id
vec3 get_normal_vector_at_vid(int vid) {
  return vec3(local_vertices_data[vid].normal_vector[0], local_vertices_data[vid].normal_vector[1], local_vertices_data[vid].normal_vector[2]);
}

// @param vid = vertex id
// @return texture coordinates for a given vertex id
vec2 get_tex_coords_at_vid(int vid) {
  return vec2(local_vertices_data[vid].tex_coords[0], local_vertices_data[vid].tex_coords[1]);
}
