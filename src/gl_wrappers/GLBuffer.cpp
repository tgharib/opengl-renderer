#include "GLBuffer.h"

// handle is initialized by glCreateBuffers
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init)
GLBuffer::GLBuffer(GLsizeiptr size, const void *data, GLbitfield flags) {
  glCreateBuffers(1, &handle);
  glNamedBufferStorage(handle, size, data, flags);
}

GLBuffer::~GLBuffer() { glDeleteBuffers(1, &handle); }
